# Guia Git (Principiantes)
<br>

<div align="center"><img src="/assets/logo.svg" width=50%"/> </div>




**Indice** <a name="indice"/>

- <a href=#1>¿Que es git?</a>
- <a href=#2>¿Que es el Control de versiones?</a>
- <a href=#3>Instalacion de Git</a>
- <a href=#4>comandos Basicos</a>
- <a href=#5>Deshacer cambios</a>
- <a href=#6>ramas</a>
- <a href=#7>Repositorios Remotos</a>





## ¿Que es Git? <a id="1"/> <a href="#indice">Volver</a>
Git es un sistema de control de versiones gratuito y de código abierto, creado originalmente por Linus Torvalds en 2005. A diferencia de los antiguos sistemas centralizados de control de versiones, como SVN y CVS, Git está distribuido: cada desarrollador tiene el historial completo de su repositorio de código de manera local. De este modo, el clon inicial del repositorio es más lento, pero las operaciones posteriores, como las confirmaciones, objeciones, diferencias, fusiones y registros, son mucho más rápidas.

## ¿Qué es el control de versiones? <a name="2"/> <a href="#indice">Volver</a>
Los sistemas de control de versiones son una categoría de herramientas de software que ayudan a un equipo de software a gestionar los cambios en el código fuente a lo largo del tiempo. El software de control de versiones realiza un seguimiento de todas las modificaciones en el código en un tipo especial de base de datos. Si se comete un error, los desarrolladores pueden ir atrás en el tiempo y comparar las versiones anteriores del código para ayudar a resolver el error al tiempo que se minimizan las interrupciones para todos los miembros del equipo.

<br>

## Instalacion de Git <a name="3"/> <a href="#indice">Volver</a>
### macOS
**Mediante Homebrew**


Instale [homebrew](https://brew.sh/) desde el **Terminal** si aún no lo tiene:
`/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"`


Despues instale Git:
`$ brew install git`

Al finalizar escribe el siguiente comando para comprobar que la instalación se ha realizado correctamente: `git --version`

```
$ git --version
```

```
 git version 2.31.1
```
>En este caso la version 2.31.1 es la ultima al momento de hacer esta publicacion


### Windows

-   Descárgate el [instalador de Git para Windows](https://git-for-windows.github.io/) más reciente.

-   Cuando hayas iniciado correctamente el instalador, deberías ver la pantalla del asistente de **configuración de Git**. Selecciona las opciones **Next** (Siguiente) y **Finish** (Finalizar) para completar la instalación. Las opciones predeterminadas son las más lógicas en la mayoría de los casos.

-   Abre el símbolo del sistema (o Git Bash si durante la instalación seleccionaste no usar Git desde el símbolo del sistema de Windows).

-   Ejecuta los siguientes comandos para configurar tu nombre de usuario y dirección de correo electrónico de Git; sustituye el nombre de Emma por el tuyo. Esta información se asociará a todas las confirmaciones que crees:

 ```
$ git config --global user.name "John Smith" git config --global user.email "eparis@atlassian.com"
```



### Linux (Basados en Debian)

Los paquetes de Git están disponibles mediante [apt](https://wiki.debian.org/Apt):

1.  Desde tu shell, instala Git mediante apt-get:

    ```
    $ sudo apt-get update
    ```

    ```
    $ sudo apt-get install git
    ```

2.  Escribe `git --version` para verificar la versión y que esta se haya instalado correctamente:

    ```
    $ git --version
    ```

    ```
    git version 2.31.1
    ```
>En este caso la version 2.31.1 es la ultima al momento de hacer esta publicacion

3.  Configura tu nombre de usuario y tu correo electrónico de Git mediante los siguientes comandos (sustituye el nombre de Emma por el tuyo). Esta información se asociará a todas las confirmaciones que crees:

    ```
    $ git config --global user.name "Emma Paris"
    ```

    ```
    $ git config --global user.email "eparis@atlassian.com"
    ```

## Comandos Basicos de Git <a name="4"/> <a href="#indice">Volver</a>

`$ Git init`

Crea un repositorio local. (Ingresa el comando sin argumentos para usar la carpeta actual como repositorio)

`$ git clone <repo>`

Clona un repositorio alojado en un repositorio remoto via HTTP o SSH


`$ git config user.name <name>`

Define el nombre del autor para los Commits

`$ git add <directory>`

Lleva los archivos al Stage <directory> para el siguiente commit. Reemplace <directory> Por cualquier Archivo dentro de la carpeta del repositorio.

`$ git commit -m "<message>"`

Comenta los cambios que hayas realizado en el proyecto y los deja listos para ser llevados al repositorio


`$ git status`

Lista los archivos que han sido modificados y nuevos para poder ser llevados al Stage

`$ git log`

Muestra el historial de cambios de todo el repositorio

`$ git diff`

Lista los cambios no trackedos entre la carpeta del proyecto y el repositorio


## Deshacer cambios <a name="5"/> <a href="#indice">Volver</a>

`$ git revert <commit>`

Crea un nuevo commit deshaciendo todos los cambios en el commit anteriores


`$ git reset <file>`

Remueve el archivo desde el 'Staging area' pero deja el directorio de trabajo sin cambios

`$ git clean -n`

Muestra cuales archivos han sido removidos del directorio de trabajo

## Ramas <a name="6"/> <a href="#indice">Volver</a>

`$ git branch`

Muestra las ramas del repositorio

`$ git checkout -b <branch>`

Crea una nueva rama en el repositorio

`$ git merge <branch>`

Combina las ramas hacia la rama actual

## Repositorios Remotos <a name="7"/> <a href="#indice">Volver</a>

`$ git remote add <url>`

Crea una nueva coneccion hacia el repositorio remoto

`git pull <remote>`

Trae la rama del repositorio remoto y la une junto con la rama actual de lrepositorio local

`git push <remote> <branch>`

Lleva los archivos del repositorio local hacia el repositorio remoto

<br>

---
**❮❯** with **❤︎** by **[Spectrasonic](twitter.com/spectrasonic117)**
